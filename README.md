# CHAT_REACT_NATIVE
El proyecto se puede correr con:

npm start, 
expo start, 
expo start --ios, 
expo start --android

La app en ambiente de desarrollo metro bundler corre en:
http://localhost:19002 del navegador.

## DESCRIPCIÓN:
Este proyecto fue creado con Expo init Project para como challenge frontend, consiste en una app que contiene un login, una pantalla que comprende el chat simulando a Yana y otra pestaña con los datos del usuario llamada cuenta. 

El chat consiste en una caja de texto, un botón que se deshabilita al estar vacío y manda el mensaje a la pantalla para ser visualizado. 

La pestaña cuenta consiste en simular la foto del usuario, nombre y mail extraídos de redux. Y un botón que cierra sesión y limpia los datos del storage para que el ciclo vuelva a comenzar. 

## NOTAS: 
En un futuro me gustaría agregar una pequeña animación para el diálogo de Yana y por supuesto, mejorar algunas prácticas en cuanto a código se refiere.


