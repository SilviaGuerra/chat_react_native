import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { ChatScreen } from "../screens/ChatScreen";
import { AccountScreen } from "../screens/AccountScreen";

const Tab = createMaterialTopTabNavigator();

export const TopTabNavigation = () => {
  return (
    <Tab.Navigator
      sceneContainerStyle={{
        backgroundColor: "#fff",
      }}
    >
      <Tab.Screen name="Chat" component={ChatScreen} />
      <Tab.Screen name="Cuenta" component={AccountScreen} />
    </Tab.Navigator>
  );
};
