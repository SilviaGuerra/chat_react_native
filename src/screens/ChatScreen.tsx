import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { dataMessages } from "../redux/messageUser"
import { useAppDispatch } from "../redux/hooks";

export const ChatScreen = () => {
  const [messageUser, setMessageUser] = useState("");
  const dispatch = useAppDispatch();

  const sendMessage = () => {
    const messagesArray = []
    messagesArray.push(messageUser)
    dispatch(dataMessages(messagesArray))
    return console.log("dineria", messagesArray);
  };

  return (
    <View style={styles.container}>
      <View style={styles.conversation}>
        <Text style={styles.date}>mar 18 de enero 22:07 PM</Text>
        <TextInput style={styles.yana} value="Hola humano" />
        <TextInput style={styles.userMessage} value="Hola Yana" />
        <TextInput style={styles.yana} value="Hola humano" />
        <TextInput style={styles.userMessage} value="Hola Yana" />
      </View>
      <View style={styles.message}>
        <TextInput style={styles.input} placeholder="Ingresa aquí tu mensaje" value={messageUser} onChangeText={(text) => setMessageUser(text)} />
        <TouchableOpacity onPress={sendMessage} style={styles.buttonMessage}>
          <Text>s</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
  },
  conversation: {},
  date: {
    color: "#6C8080",
    fontSize: 12,
    lineHeight: 16,
    textAlign: "center",
    padding: 5,
  },
  yana: {
    backgroundColor: "#F0F6FA",
    padding: 8,
    width: "50%",
    borderRadius: 26,
    margin: 5,
    textAlign: "center",
  },
  userMessage: {
    backgroundColor: "#3B9391",
    width: "50%",
    borderRadius: 28,
    color: "#fff",
    padding: 10,
    margin: 5,
    alignSelf: "flex-end",
    textAlign: "center",
  },
  message: {
    flexDirection: "row",
  },
  input: {
    flex: 8,
    flexDirection: "row",
    alignItems: "flex-end",
    borderWidth: 1,
    borderColor: "#F0F6FA",
    borderRadius: 28,
    padding: 10,
    margin: 5,
    shadowColor: "#000",
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  buttonMessage: {
    flex: 2,
    backgroundColor: "#C4C4C4",
  },
});
