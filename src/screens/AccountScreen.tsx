import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";

export const AccountScreen = () => {
  return (
    <View style={styles.container}>
        <Text style={styles.user}/>
      <Text>Nombre de usuario</Text>
      <Text>yo</Text>
      <Text>Correo electrónico</Text>
      <Text>test@gmail.com</Text>
      <TouchableOpacity>
          <Text>Cerrar Sesión</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    user: {
        width: 96,
        height: 96,
        backgroundColor: '#C4C4C4',
        borderRadius: 10
    }
})

