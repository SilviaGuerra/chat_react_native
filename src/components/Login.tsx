import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useAppDispatch } from "../redux/hooks";
import { dataName, dataEmail } from "../redux/infoUser";
// import ButtonStyled from "./ButtonStyled";
// import { createGlobalStyle } from "styled-components"
import styled from "styled-components";

const ButtonStyled = styled(TouchableOpacity)`
  align-items: center;
  justify-content: center;
  background-color: #ff8755;
  padding: 10px;
  border-radius: 40px;
  height: 64px;
`;

export const Login = (props) => {
  const { navigation } = props;
  const [userName, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const dispatch = useAppDispatch();

  const goToChat = () => {
    navigation.navigate("Tabs");
  };

  const onPress = () => {
    dispatch(dataName(userName));
    dispatch(dataEmail(userEmail));
    goToChat();
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#fff",
      }}
    >
      <Text style={styles.titleHeader}>Regístrate</Text>
      <TextInput
        style={styles.input}
        placeholder="Nombre de usuario"
        value={userName}
        onChangeText={(text) => setUserName(text)}
        autoCapitalize="none"
      />
      <TextInput
        style={styles.input}
        placeholder="Correo electrónico"
        value={userEmail}
        onChangeText={(text) => setUserEmail(text)}
        autoCapitalize="none"
      />
      <TextInput
        style={styles.input}
        placeholder="Contraseña"
        secureTextEntry={true}
        autoCapitalize="none"
      />
      <ButtonStyled onPress={onPress}>
        <Text style={styles.textButton}>Crear cuenta</Text>
      </ButtonStyled>
    </View>
  );
};

const styles = StyleSheet.create({
  titleHeader: {
    textAlign: "center",
    fontWeight: "900",
    fontSize: 40,
    fontFamily: "sf-pro-display-bold",
  },
  input: {
    height: 64,
    borderRadius: 32,
    backgroundColor: "#F0F6FA",
    padding: 10,
    margin: 10,
    fontFamily: "sf-pro-display-regular",
  },
  textButton: {
    color: "#672A11",
    fontSize: 16,
    fontFamily: "sf-pro-display-bold",
  },
});
