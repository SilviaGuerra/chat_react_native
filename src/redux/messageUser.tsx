import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface messages {
    messagesUser: any
}

const initialState: messages = {
    messagesUser: []
}

const messageUser = createSlice({
    name: "messages",                                                                                                       
    initialState,
    reducers: {
        dataMessages: (state, action: PayloadAction<any>) => {
            state.messagesUser = action.payload
        }
    }
});
console.log(messageUser, "dataMensajes")

export const { dataMessages } = messageUser.actions;
export default messageUser.reducer;
