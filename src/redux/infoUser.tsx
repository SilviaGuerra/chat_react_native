import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface infoUserState {
  name: string;
  email: string;
  login: boolean;
}

const initialState: infoUserState = {
  name: "",
  email: "",
  login: false,
};

const infoUser = createSlice({
  name: "infoUser",
  initialState,
  reducers: {
    dataName: (state, action: PayloadAction<string>) => {
      console.log("mis ojos", action.payload);
      state.name = action.payload;
    },
    dataEmail: (state, action: PayloadAction<string>) => {
      state.email = action.payload;
    },
    reset: (state) => {
      state.name = "";
      state.email = "";
      state.login = false;
    },
  },
});

export const { dataName, dataEmail, reset } = infoUser.actions;
export default infoUser.reducer;
