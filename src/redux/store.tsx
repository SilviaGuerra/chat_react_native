import { configureStore } from "@reduxjs/toolkit";
import infoUser from "./infoUser";
import messageUser from './messageUser';

export const store = configureStore({
  reducer: {
    dataUser: infoUser,
    dataChat: messageUser
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
