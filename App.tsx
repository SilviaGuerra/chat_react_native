import { NavigationContainer } from "@react-navigation/native";
import { Provider } from "react-redux";
import { store } from "./src/redux/store";
import { NavigationStack } from "./src/navigation/NavigationStack";
import AppLoading from "expo-app-loading";
import { useFonts } from "@expo-google-fonts/raleway";

const App = () => {
  let [fontsLoaded] = useFonts({
    // Raleway_400Regular,
    // Raleway_700Bold,
    "sf-pro-display-regular": require("./assets/fonts/SFProDisplay-Regular.ttf"),
    "sf-pro-display-bold": require("./assets/fonts/SFProDisplay-Bold.ttf"),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <NavigationStack />
        </NavigationContainer>
      </Provider>
    );
  }
};

export default App;
